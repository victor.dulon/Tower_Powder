extends Node2D

##Replace with Description

### ENUM
enum Etat_Preview {AUCUNE = 0, MUR = 1, TIREUR = 2, COEUR = 3, PLATEFORME = 4}

### SIGNAL

### EXPORT VARIABLE
@export_range(0,1) var index_joueur : int = 0

@export_category("VARIABLES EXPORT")
@export var _animation_player_achat : AnimationPlayer
@export var _label_achat : Label
### PUBLIC VARIABLE


### PRIVATE VARIABLE
var _input_velocity = Vector2.ZERO
var _speed : int = ProjectData.vitesse_mouvement_controlleur

var _curseur_sprite2D : Sprite2D
var _animation_player : AnimationPlayer
var _area2D : Area2D
var _previsualisation_bloc : Node2D

var _heart_spawned : bool = false

var _etat_preview : Etat_Preview = Etat_Preview.AUCUNE
var _instance
var _en_rotation : bool = false
var _input_canon_orientation : Vector2
var _input_canon_orientation_02 : Vector2

var _bouton_Y_appuye : bool = false
var _bouton_gauche_appuye : bool = false
var _bouton_haut_appuye : bool = false

var _variation_mur_en_cours : int = 0
var _variation_tireur_en_cours : int = 0
var _instance_achetee

var _current_rotation : int = 0
var _tween : Tween

### INHERITED METHOD
func _ready():
	_Assignation_Variables()
	_Colorisation_Curseur()
	_Connect_Signals()
	pass

func _process(delta):
	if StateMachine.Get_State_Curseur_Depuis_Index(index_joueur) == StateMachine.Etat_Curseur.LIBRE :
		# Bouger le curseur
		_input_velocity = Vector2(Input.get_joy_axis(index_joueur, JOY_AXIS_LEFT_X), Input.get_joy_axis(index_joueur, JOY_AXIS_LEFT_Y))
		_input_velocity = GameManager.Apply_Joy_Deadzone(_input_velocity) 
		position += _input_velocity * _speed * delta
		
		_Limitation_Position()
	

	# Orienter le canon de l'instance en fonction du joystick droit
	if _instance != null:
		var temp_input_canon_orientation = Vector2(Input.get_joy_axis(index_joueur, JOY_AXIS_RIGHT_X), Input.get_joy_axis(index_joueur, JOY_AXIS_RIGHT_Y))
		if temp_input_canon_orientation.length() > 0.8:
			_input_canon_orientation = temp_input_canon_orientation
		
		_instance.Rotation_Canon(_input_canon_orientation)



func _input(event):
	if StateMachine.current_etat_jeu == StateMachine.Etat_Jeu.PLACEMENT and StateMachine.Get_State_Curseur_Depuis_Index(index_joueur) == StateMachine.Etat_Curseur.LIBRE:
		# Gestion des apparitions des previews
		if Input.is_joy_button_pressed(index_joueur, JOY_BUTTON_DPAD_LEFT) and !_bouton_gauche_appuye:
			_bouton_gauche_appuye = true
			_Clean_Previsualisation(false)
			_Show_Sprite_2D(false)
			var _mur_a_instantier
			if _variation_mur_en_cours == 0:
				_variation_mur_en_cours += 1
				_mur_a_instantier = Bibliotheque.bloc_mur
			elif _variation_mur_en_cours == 1:
				_variation_mur_en_cours += 1
				_mur_a_instantier = Bibliotheque.bloc_mur_B
			elif _variation_mur_en_cours == 2:
				_variation_mur_en_cours += 1
				_mur_a_instantier = Bibliotheque.bloc_mur_C
			elif _variation_mur_en_cours == 3:
				_variation_mur_en_cours += 1
				if index_joueur == 0 : _mur_a_instantier = Bibliotheque.bloc_mur_D
				else : _mur_a_instantier = Bibliotheque.bloc_mur_D_Droite
			elif _variation_mur_en_cours == 4:
				_variation_mur_en_cours = 1
				_mur_a_instantier = Bibliotheque.bloc_mur
		
			_Instantier_Previsualisation(_mur_a_instantier)
			_etat_preview = Etat_Preview.MUR
			await get_tree().create_timer(0.1).timeout
			_bouton_gauche_appuye = false
		
		if Input.is_joy_button_pressed(index_joueur, JOY_BUTTON_DPAD_UP) and !_bouton_haut_appuye:
			_bouton_haut_appuye = true
			_Clean_Previsualisation(false)
			_Show_Sprite_2D(false)
			var _tireur_a_instantier
			if _variation_tireur_en_cours == 0:
				_variation_tireur_en_cours += 1
				_tireur_a_instantier = Bibliotheque.bloc_tireur
			elif _variation_tireur_en_cours == 1:
				_variation_tireur_en_cours = 0
				_tireur_a_instantier = Bibliotheque.bloc_tireur_B
			
			_Instantier_Previsualisation(_tireur_a_instantier)
			_etat_preview = Etat_Preview.TIREUR
			await get_tree().create_timer(0.1).timeout
			_bouton_haut_appuye = false
		
		if Input.is_joy_button_pressed(index_joueur, JOY_BUTTON_DPAD_RIGHT) and _heart_spawned == false :
			_Clean_Previsualisation(false)
			_Show_Sprite_2D(false)
			_Instantier_Previsualisation(Bibliotheque.bloc_coeur)
			_etat_preview = Etat_Preview.COEUR
		
		if Input.is_joy_button_pressed(index_joueur, JOY_BUTTON_DPAD_DOWN) and _etat_preview != Etat_Preview.PLATEFORME :
			_Clean_Previsualisation(false)
			_Show_Sprite_2D(false)
			_Instantier_Previsualisation(Bibliotheque.bloc_plateforme)
			_etat_preview = Etat_Preview.PLATEFORME
		
		#if Input.is_joy_button_pressed(index_joueur, JOY_BUTTON_DPAD_DOWN) and _etat_preview != Etat_Preview.AUCUNE and StateMachine.current_game_state == StateMachine.Game_State.PLACEMENT:
			#_Clean_Previsualisation(false)
		
		# Gestion des tours
		if Input.is_joy_button_pressed(index_joueur,JOY_BUTTON_Y) and RuntimeData.game_ready_car_coeurs_spawned:
			if !_bouton_Y_appuye :
				_bouton_Y_appuye = true
				_Clean_Previsualisation(false)
				await get_tree().create_timer(0.1).timeout
				EventManager.Invoke_On_Commencement_Du_Tour()
		
		# Gestion de la création des blocs
		if Input.is_joy_button_pressed(index_joueur,JOY_BUTTON_X) and _etat_preview != Etat_Preview.AUCUNE :
			if _Est_Capable_Dacheter():
				_Show_Sprite_2D(true)
				_etat_preview = Etat_Preview.AUCUNE
				_Creer_Bloc()
				_Clean_Previsualisation(true)
			else :
				# intégrer ici un bruit et une animation de refus
				_tween = create_tween()
				_tween.tween_property(_instance, "modulate", Color.RED, 0.1)
				_tween.tween_property(_instance, "modulate", Color.WHITE, 0.1)


### SIGNAL METHOD
func _Connect_Signals():
	EventManager._on_commencement_du_tour.connect(_Clean_Previsualisation.bind(false))
	EventManager._on_fin_du_tour.connect(_on_fin_du_tour)
	pass

### PUBLIC METHOD
func Descactivation_Temporaire():
	_animation_player.play("Desactivation_Temporaire")

### PRIVATE METHOD
func _Colorisation_Curseur():
	if index_joueur == 0:
		_curseur_sprite2D.modulate = Color.DODGER_BLUE
	elif index_joueur == 1:
		_curseur_sprite2D.modulate = Color.INDIAN_RED

func _Assignation_Variables():
	var enfants = self.get_children()
	for enfant in enfants :
		if enfant is Sprite2D and enfant.name == "Playeur_Curseur_Sprite":
			_curseur_sprite2D = enfant
		elif enfant is AnimationPlayer:
			_animation_player = enfant
		elif enfant is Area2D:
			_area2D = enfant
		elif enfant.get_meta("Type") == "Previsualiation_Bloc":
			_previsualisation_bloc = enfant
		
		_label_achat.visible = false

func _Clean_Previsualisation(achat : bool):
	if achat :
		_Lancer_Animation_Achat()
	var enfants  = _previsualisation_bloc.get_children()
	for enfant in enfants:
		if enfant != _instance_achetee:
			enfant.queue_free()
	_current_rotation = 1


func _Instantier_Previsualisation(bloc : PackedScene):
	_instance = bloc.instantiate()
	_previsualisation_bloc.add_child(_instance)
	_instance.Freeze(true)
	_Lancer_Animation(_instance)
	_instance._Ajouter_Preview_Prix()
	_instance.Animation_Apparition()

func _Creer_Bloc():
	var type = _instance.get_meta("Type")
	if type == "Coeur" :
		_heart_spawned = true
		EventManager.Invoke_On_Spawn_Coeur(index_joueur)
	GameManager.Create_Bloc(global_position, type, _input_canon_orientation, index_joueur)
	var prix = GameManager.Get_Prix_Grace_Aux_Metas(_instance)
	_Remise_A_Zero_Compteurs()
	EventManager.Invoke_On_Variation_Argent(index_joueur, -prix)

func _Est_Capable_Dacheter() -> bool:
	var prix = GameManager.Get_Prix_Grace_Aux_Metas(_instance)
	if index_joueur == 0 and RuntimeData.argent_joueur_01 >= prix:
		return true
	elif index_joueur == 1 and RuntimeData.argent_joueur_02 >= prix:
		return true
	else : 
		return false

func _Lancer_Animation(instance : Node2D):
	if instance.get_meta("Type") == "Tireur" or instance.get_meta("Type") == "Tireur_B":
		if _etat_preview == Etat_Preview.TIREUR:
			instance.Play("Apparition")
		elif _etat_preview == Etat_Preview.AUCUNE:
			instance.Play("Idle")

func _on_fin_du_tour():
	_bouton_Y_appuye = false

func _Remise_A_Zero_Compteurs():
	_variation_tireur_en_cours = 0
	_variation_mur_en_cours = 0
	_etat_preview = Etat_Preview.AUCUNE

func _Show_Sprite_2D(show : bool):
	_curseur_sprite2D.visible = show

func _Lancer_Animation_Achat():
	_label_achat.text = "- " + str(GameManager.Get_Prix_Grace_Aux_Metas(_instance)) + " €"
	_animation_player_achat.play("Achat")

func _Limitation_Position():
	if index_joueur == 0:
		if position.x > 575 : position.x = 575
		if position.x < 0 : position.x = 0
		if position.y > 650 : position.y = 650
		if position.y < 0 : position.y = 0
	else :
		if position.x > 1150 : position.x = 1150
		if position.x < 575 : position.x = 575
		if position.y > 650 : position.y = 650
		if position.y < 0 : position.y = 0
