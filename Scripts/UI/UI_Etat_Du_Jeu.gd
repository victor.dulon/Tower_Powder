extends Control

##Replace with Description

### ENUM

### SIGNAL

### EXPORT VARIABLE
@export var label_etat_du_jeu : Label
@export var bouton_relancer_partie : Button
@export var label_timer : Label

### PUBLIC VARIABLE

### PRIVATE VARIABLE
var _timer_on : bool = true

### INHERITED METHOD
func _ready():
	_Connect_Signals()
	_Mise_En_Place_Nodes()
	pass

### SIGNAL METHOD
func _Connect_Signals():
	EventManager._on_commencement_du_tour.connect(_on_commencement_du_tour)
	EventManager._on_fin_du_tour.connect(_on_fin_du_tour)
	EventManager._on_defaite_joueur.connect(_on_defaite_joueur)
	EventManager._on_nouvelle_partie.connect(_on_fin_du_tour)
	
	bouton_relancer_partie.pressed.connect(_on_bouton_relancer_partie_pressed)


### PUBLIC METHOD

### PRIVATE METHOD
func _on_commencement_du_tour():
	label_etat_du_jeu.text = "FIGHT !"
	label_timer.visible = true
	_timer_on = true
	Start_Timer()

func _on_fin_du_tour():
	Stop_Timer()
	label_etat_du_jeu.text = "PLACEZ VOS PIECES"

func _on_defaite_joueur(index_joueur : int):
	Stop_Timer()
	bouton_relancer_partie.visible = true
	if index_joueur == 0:
		label_etat_du_jeu.text = "JOUEUR 02 GAGNE"
	else :
		label_etat_du_jeu.text = "JOUEUR 01 GAGNE"

func _on_bouton_relancer_partie_pressed():
	GameManager.Go_To_Choix_Terrain()
	bouton_relancer_partie.visible = false

func Start_Timer():
	if _timer_on:
		await get_tree().create_timer(1).timeout
		if RuntimeData.temps_restant == 0 :
			EventManager.Invoke_On_Fin_Du_Tour()
			return
		RuntimeData.temps_restant -= 1
		label_timer.text = str(RuntimeData.temps_restant) + " s"
		Start_Timer()

func Stop_Timer():
	_timer_on = false
	label_timer.visible = false

func _Mise_En_Place_Nodes():
	bouton_relancer_partie.visible = false
	label_timer.text = str(RuntimeData.temps_restant) + " s"
	label_timer.visible = false
