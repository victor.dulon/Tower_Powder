extends Control

##Replace with Description

### ENUM

### SIGNAL

### EXPORT VARIABLE
@export_range(0,1) var index_joueur : int = 0
@export var amount_label : Label

### PUBLIC VARIABLE

### PRIVATE VARIABLE

### INHERITED METHOD
func _init():
	_Connect_Signals()
	pass

### SIGNAL METHOD
func _Connect_Signals():
	EventManager._on_mise_a_jour_argent.connect(_MAJ_Argent)
	pass

### PUBLIC METHOD

### PRIVATE METHOD
func _MAJ_Argent():
	if index_joueur == 0 :
		amount_label.text = str(RuntimeData.argent_joueur_01)
	elif index_joueur == 1 :
		amount_label.text = str(RuntimeData.argent_joueur_02)
	else:
		printerr("Mauvais index de joueur renseigné : ", index_joueur)
