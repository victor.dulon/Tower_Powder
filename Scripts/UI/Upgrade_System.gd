extends Control

##Replace with Description

### ENUM

### SIGNAL

### EXPORT VARIABLE
@export var visualisation_balle : TextureRect

### PUBLIC VARIABLE

### PRIVATE VARIABLE

### INHERITED METHOD
func _ready():
	Show(false)
	
### PUBLIC METHOD
func Show(show : bool):
	self.visible = show

func Changer_UI_Balle(type : String):
	var _node_tireur = self.get_parent().get_parent()
	var _changement_visuel_autorise : bool = true
	if _node_tireur.get_meta("Type") != "Tireur" and _node_tireur.get_meta("Type") != "Tireur_B":
		_changement_visuel_autorise = false
		printerr("Erreur dans la tentative de changer de balle sur un node qui n'est pas un tireur agréé. Node : ", _node_tireur)
		printerr("Meta : ", _node_tireur.get_meta("Type"))
	match type:
		"Classique":
			visualisation_balle.texture = Bibliotheque.ui_balle_classique
		"Warpzone":
			visualisation_balle.texture = Bibliotheque.ui_balle_warpzone
		"Gravite Inversee":
			visualisation_balle.texture = Bibliotheque.ui_balle_gravite_inversee
		"Rocket":
			visualisation_balle.texture = Bibliotheque.ui_balle_rocket
	if _changement_visuel_autorise : _node_tireur.Changer_Visuel_Oeil(type)
