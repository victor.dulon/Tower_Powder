extends Control

##Replace with Description

### ENUM

### SIGNAL

### EXPORT VARIABLE
@export var index_joueur : int

### PUBLIC VARIABLE

### PRIVATE VARIABLE
var _premier_bouton_focused : VBoxContainer
var _grid_container : GridContainer
var _boutons : Array[VBoxContainer]
var _focused_bouton : VBoxContainer


var _mouvement
var _autorise_a_bouger : bool = true
var _terrain_choisi : bool = false

### INHERITED METHOD
func _ready():
	_Assigner_Variables()
	_Connect_Signals()
	pass

func _input(event):	
		if Input.is_joy_button_pressed(index_joueur, JOY_BUTTON_DPAD_LEFT) and _autorise_a_bouger and !_terrain_choisi:
			Move("Gauche")
			
		if Input.is_joy_button_pressed(index_joueur, JOY_BUTTON_DPAD_RIGHT) and _autorise_a_bouger and !_terrain_choisi:
			Move("Droite")
		
		if Input.is_joy_button_pressed(index_joueur, JOY_BUTTON_DPAD_UP) and _autorise_a_bouger and !_terrain_choisi:
			Move("Haut")
			
		if Input.is_joy_button_pressed(index_joueur, JOY_BUTTON_DPAD_DOWN) and _autorise_a_bouger and !_terrain_choisi:
			Move("Bas")
		
		if Input.is_joy_button_pressed(index_joueur, JOY_BUTTON_A) and !_terrain_choisi:
			Select()
		
		if Input.is_joy_button_pressed(index_joueur, JOY_BUTTON_B) and _terrain_choisi:
			Unselect()

### SIGNAL METHOD
func _Connect_Signals():
	pass

### PUBLIC METHOD
func Move(direction : String):
	_focused_bouton = _focused_bouton.Move(direction)
	_autorise_a_bouger = false
	await get_tree().create_timer(0.1).timeout
	_autorise_a_bouger = true

func Select():
	_terrain_choisi = true
	_focused_bouton.Selected(index_joueur)

func Unselect():
	_terrain_choisi = false
	_focused_bouton.Unselect(index_joueur)

	
### PRIVATE METHOD
func _Assigner_Variables():
	_grid_container = self.get_child(0)
	var enfants = _grid_container.get_children()
	for enfant in enfants :
		if enfant is VBoxContainer and enfant.est_focus_en_premier :
			_focused_bouton = enfant
			_focused_bouton.Focus(true)
		if enfant is VBoxContainer:
			_boutons.append(enfant)
