extends VBoxContainer

##Replace with Description

### ENUM

### SIGNAL

### EXPORT VARIABLE
@export var est_focus_en_premier : bool = false
@export var terrain : ProjectData.Type_Terrain = ProjectData.Type_Terrain.TERRAIN_01

@export var button_left : VBoxContainer
@export var button_right : VBoxContainer
@export var button_up : VBoxContainer
@export var button_down : VBoxContainer

### PUBLIC VARIABLE

### PRIVATE VARIABLE
var _texture_button : TextureRect
var _texture_normal_texture_button : CompressedTexture2D
var _texture_focus_texture_button : CompressedTexture2D
var _texture_selected_texture_button : CompressedTexture2D
var _prix_label : Label

var is_focused : bool = false

### INHERITED METHOD
func _ready():
	_Assignation_Variables()
	_Connect_Signals()
	pass

### SIGNAL METHOD
func _Connect_Signals():
	pass

### PUBLIC METHOD
func Focus(focus : bool):
	if focus :
		is_focused = true
		_texture_button.texture = _texture_focus_texture_button
		_prix_label.set("theme_override_colors/font_color", Bibliotheque.terrain_focus_color)
	else:
		is_focused = false
		_texture_button.texture = _texture_normal_texture_button
		_prix_label.set("theme_override_colors/font_color", Bibliotheque.terrain_normal_color)

func Move(direction : String) -> VBoxContainer:
	if is_focused :
		if direction == "Haut" and button_up != null:
			button_up.Focus(true)
			self.Focus(false)
			return button_up
		elif direction == "Bas" and button_down != null:
			button_down.Focus(true)
			self.Focus(false)
			return button_down
		elif direction == "Droite" and button_right != null:
			button_right.Focus(true)
			self.Focus(false)
			return button_right
		elif direction == "Gauche" and button_left != null:
			button_left.Focus(true)
			self.Focus(false)
			return button_left
		else : 
			return self
	else :
		return self

func Selected(_index_joueur : int):
	_texture_button.texture = _texture_selected_texture_button
	_prix_label.set("theme_override_colors/font_color", Bibliotheque.terrain_selected_color)
	EventManager.Invoke_On_Choix_Terrain(terrain, _index_joueur, true)

func Unselect(_index_joueur : int):
	_texture_button.texture = _texture_focus_texture_button
	_prix_label.set("theme_override_colors/font_color", Bibliotheque.terrain_focus_color)
	EventManager.Invoke_On_Choix_Terrain(terrain, _index_joueur, false)

### PRIVATE METHOD
func _Assignation_Variables():
	var enfants = self.get_children()
	for enfant in enfants :
		if enfant is TextureRect and _texture_button == null:
			_texture_button = enfant
			_Assigner_Texture_Button_Images()
		elif enfant is Label and _prix_label == null:
			_prix_label = enfant
			_Assigner_Prix()

func _Assigner_Texture_Button_Images():
	match terrain:
		1:
			_texture_normal_texture_button = Bibliotheque.terrain_01_button_image
			_texture_focus_texture_button = Bibliotheque.terrain_01_button_focus_image
			_texture_selected_texture_button = Bibliotheque.terrain_01_button_selected_image
		2:
			_texture_normal_texture_button = Bibliotheque.terrain_02_button_image
			_texture_focus_texture_button = Bibliotheque.terrain_02_button_focus_image
			_texture_selected_texture_button = Bibliotheque.terrain_02_button_selected_image
		3:
			_texture_normal_texture_button = Bibliotheque.terrain_03_button_image
			_texture_focus_texture_button = Bibliotheque.terrain_03_button_focus_image
			_texture_selected_texture_button = Bibliotheque.terrain_03_button_selected_image
		4:
			_texture_normal_texture_button = Bibliotheque.terrain_04_button_image
			_texture_focus_texture_button = Bibliotheque.terrain_04_button_focus_image
			_texture_selected_texture_button = Bibliotheque.terrain_04_button_selected_image
		5:
			_texture_normal_texture_button = Bibliotheque.terrain_05_button_image
			_texture_focus_texture_button = Bibliotheque.terrain_05_button_focus_image
			_texture_selected_texture_button = Bibliotheque.terrain_05_button_selected_image
		6:
			_texture_normal_texture_button = Bibliotheque.terrain_06_button_image
			_texture_focus_texture_button = Bibliotheque.terrain_06_button_focus_image
			_texture_selected_texture_button = Bibliotheque.terrain_06_button_selected_image
		7:
			_texture_normal_texture_button = Bibliotheque.terrain_07_button_image
			_texture_focus_texture_button = Bibliotheque.terrain_07_button_focus_image
			_texture_selected_texture_button = Bibliotheque.terrain_07_button_selected_image
		8:
			_texture_normal_texture_button = Bibliotheque.terrain_08_button_image
			_texture_focus_texture_button = Bibliotheque.terrain_08_button_focus_image
			_texture_selected_texture_button = Bibliotheque.terrain_08_button_selected_image
		9:
			_texture_normal_texture_button = Bibliotheque.terrain_09_button_image
			_texture_focus_texture_button = Bibliotheque.terrain_09_button_focus_image
			_texture_selected_texture_button = Bibliotheque.terrain_09_button_selected_image
			
	_texture_button.texture = _texture_normal_texture_button

func _Assigner_Prix():
	var revenus : int
	match terrain:
		1: revenus = ProjectData.revenus_terrain_01
		2: revenus = ProjectData.revenus_terrain_02
		3: revenus = ProjectData.revenus_terrain_03
		4: revenus = ProjectData.revenus_terrain_04
		5: revenus = ProjectData.revenus_terrain_05
		6: revenus = ProjectData.revenus_terrain_06
		7: revenus = ProjectData.revenus_terrain_07
		8: revenus = ProjectData.revenus_terrain_08
		9: revenus = ProjectData.revenus_terrain_09
	_prix_label.text = "Revenus : " + str(revenus)
