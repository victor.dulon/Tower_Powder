extends Control

##Replace with Description

### ENUM

### SIGNAL

### EXPORT VARIABLE
@export var label_prix : Label
@export var label_prix_depense : Label
@export var preview_global_margin : MarginContainer

### PUBLIC VARIABLE

### PRIVATE VARIABLE
var _tween : Tween

### INHERITED METHOD
func _ready():
	_Connect_Signals()
	_Set_Up_Graphique()
	_Renseigner_Le_Prix()
	pass

### SIGNAL METHOD
func _Connect_Signals():
	pass

### PUBLIC METHOD
func Animation_Apparition():
	_tween = create_tween()
	_tween.tween_property(self, "rotation_degrees", -20, 0.15)
	_tween.tween_property(self, "rotation_degrees", 12, 0.10)
	_tween.tween_property(self, "rotation_degrees", 0, 0.08)

func Animation_Achat():
	_tween = create_tween().set_parallel()
	_tween.tween_property(label_prix_depense, "modulate", Color(1,1,1,1), 0.4)
	_tween.tween_property(label_prix_depense, "position", Vector2(29,-36), 1.5)
	_tween.tween_property(preview_global_margin, "scale", Vector2.ZERO, 0.2)
	await get_tree().create_timer(0.4).timeout
	_tween = create_tween()
	_tween.tween_property(label_prix_depense, "modulate", Color(1,1,1,0), 0.1)

### PRIVATE METHOD
func _Renseigner_Le_Prix():
	label_prix.text = str(GameManager.Get_Prix_Grace_Aux_Metas(self.get_parent())) + " €"
	label_prix_depense.text = "- " + label_prix.text

func _Set_Up_Graphique():
	label_prix_depense.modulate = Color(1,1,1,0)
