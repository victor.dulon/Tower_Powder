extends Node2D

##Replace with Description

### ENUM

### SIGNAL

### EXPORT VARIABLE
@export var marker_Origine : Marker2D
@export var marker_Destination : Marker2D

### PUBLIC VARIABLE
var orientation : Vector2
var balle : PackedScene

### PRIVATE VARIABLE
var _sprite2D : Sprite2D
var _is_shooting : bool = false
var _is_allowed_to_shoot : bool = false
var bloc_parent : Node2D


### INHERITED METHOD
func _ready():
	_Assignation_Variables()
	_Connect_Signals()
	pass

### SIGNAL METHOD
func _Connect_Signals():
	EventManager._on_commencement_du_tour.connect(_Start_Shooting)
	EventManager._on_fin_du_tour.connect(_Stop_Shooting.bind(0))
	EventManager._on_defaite_joueur.connect(_Stop_Shooting)
	pass

### PUBLIC METHOD
func Shoot():
	if StateMachine.current_etat_jeu == StateMachine.Etat_Jeu.AFFRONTEMENT and !_is_shooting and _is_allowed_to_shoot:
		_is_shooting = true
		var instance = balle.instantiate()
		var rigidbody : RigidBody2D
		var enfants = instance.get_children()
		for enfant in enfants :
			if enfant is RigidBody2D:
				rigidbody = enfant
		instance.global_position = marker_Origine.global_position
		instance.index_joueur = bloc_parent.index_joueur
		instance.Rotation_Balle(get_orientation_vector())
		GameManager.receiverNode.add_child(instance)
		
		rigidbody.apply_central_impulse(get_orientation_vector() * Force_Tir_En_Fonction_Joueur())
		
		await get_tree().create_timer(RuntimeData.action_tir_cool_down).timeout
		_is_shooting = false
		Shoot()

func Changer_Balle(type : String):
	balle = GameManager.Type_Balle_Depuis_Nom(type)

### PRIVATE METHOD
func _Assignation_Variables():
	var enfants = self.get_children()
	for enfant in enfants :
		if enfant is Sprite2D:
			_sprite2D = enfant
	
	balle = GameManager.Type_Balle_Depuis_Nom("Classique")

	var parent = get_parent().get_parent()
	if parent is Node2D :
		bloc_parent = parent

func get_orientation_vector() -> Vector2:
	return -(marker_Origine.global_position - marker_Destination.global_position)

func _Start_Shooting():
	_is_allowed_to_shoot = true
	Shoot()

func _Stop_Shooting(index_joueur : int):
	_is_allowed_to_shoot = false

func Force_Tir_En_Fonction_Joueur() -> int :
	var joueur = get_parent().get_parent().index_joueur
	if joueur != null :
		if joueur == 0 :
			return RuntimeData.puissance_tir_joueur_01
		else:
			return RuntimeData.puissance_tir_joueur_02
	else:
		printerr("L'identite du joueur n'est pas accessible pour la balle : ", self.name, "( joueur = ", joueur, " )")
		return 0
