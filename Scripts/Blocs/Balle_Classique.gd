extends Node2D

##Replace with Description

### ENUM

### SIGNAL

### EXPORT VARIABLE
@export var rigid_body : RigidBody2D
@export var animation_player : AnimationPlayer
@export var area2D : Area2D
@export var sprite : Sprite2D

### PUBLIC VARIABLE
var index_joueur : int = -1

### PRIVATE VARIABLE

### INHERITED METHOD
func _ready():
	_Connect_Signaux()
	
### PUBLIC METHOD
func Explosion():
	rigid_body.process_mode = Node.PROCESS_MODE_DISABLED
	animation_player.play("Explosion")

func Queue_Free():
	self.queue_free()

func Rotation_Balle(_direction : Vector2):
	print("Direction de la balle = ", _direction)
	sprite.rotation = _direction.angle()
	if index_joueur == 1 : sprite.flip_h = true

### PRIVATE METHOD
func _Connect_Signaux():
	if area2D != null : area2D.body_entered.connect(_on_body_entered)

func _on_body_entered(body):
	if "index_joueur" in body.get_parent():
		if body.get_parent().index_joueur != index_joueur:
			await get_tree().create_timer(0.03).timeout
			Explosion()
