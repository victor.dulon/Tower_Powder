extends Node2D

##Replace with Description
# il faut que je fasse en sorte que le preview prix soit checké mais pas automatique et qu'il ne fasse pas d'erreur quand il n'est pas là


### ENUM
enum ETAT_BLOC {NEUTRE = 0, EN_MODIFICATION = 10}

### SIGNAL

### EXPORT VARIABLE

### PUBLIC VARIABLE
var is_active : bool = true
var index_joueur : int 

### PRIVATE VARIABLE
var _body 
var _sprite2D : Sprite2D
var _collision_shape : CollisionShape2D
var _preview_prix : Control
var _etat_bloc : ETAT_BLOC = ETAT_BLOC.NEUTRE
var _bouton_appuye : bool = false

# Actions
var _Actions : Array[Node2D]
var _action_tir : Node2D
var _action_tir_02 : Node2D
var _input_canon_orientation : Vector2

# Cassable
var _is_cassable : bool = false
var _action_cassable : Node2D
var _action_cassable_area2D : Area2D
var _pv : int

# Upgradable
var _is_upgradable : bool = false
var _upgrade_systeme : Control
var _current_type_balle : String = "Classique"
var ball_types = ProjectData.ball_types
var _sprite_oeil : Sprite2D

var _animation_player : AnimationPlayer

var _tween : Tween

### INHERITED METHOD
func _ready():
	_Assignation_Variables()
	_Set_Up()
	_Check_If_Actions_Here()
	_Connect_Signals()

### SIGNAL METHOD
func _Connect_Signals():
	if _is_cassable : _action_cassable_area2D.body_entered.connect(_on_body_entered)
	pass

func _process(delta):
	if _etat_bloc == ETAT_BLOC.EN_MODIFICATION :
		if _action_tir != null:
			var temp_input_canon_orientation = Vector2(Input.get_joy_axis(index_joueur, JOY_AXIS_RIGHT_X), Input.get_joy_axis(index_joueur, JOY_AXIS_RIGHT_Y))
			if temp_input_canon_orientation.length() > 0.8:
				_input_canon_orientation = temp_input_canon_orientation
			Rotation_Canon(_input_canon_orientation)
		
		if Input.is_joy_button_pressed(index_joueur,JOY_BUTTON_DPAD_LEFT) and _is_upgradable and !_bouton_appuye:
			_bouton_appuye = true
			_Changer_Type_Balle("Gauche")
			await get_tree().create_timer(0.2).timeout
			_bouton_appuye = false
		
		if Input.is_joy_button_pressed(index_joueur,JOY_BUTTON_DPAD_RIGHT) and _is_upgradable and !_bouton_appuye:
			_bouton_appuye = true
			_Changer_Type_Balle("Droite")
			await get_tree().create_timer(0.2).timeout
			_bouton_appuye = false

### PUBLIC METHOD
func Affiche_Upgrade(show : bool):
	if _is_upgradable:
		_upgrade_systeme.Show(show)

func Animation_Achat():
	_preview_prix.Animation_Achat()
	if _body != null : _body.queue_free()

func Animation_Apparition():
	_preview_prix.Animation_Apparition()
	var originalScale = self.scale
	_tween = create_tween()
	_tween.tween_property(self, "scale", ProjectData.bloc_scale_anim_apparition, ProjectData.bloc_timing_anim_apparition)
	_tween.tween_property(self, "scale", originalScale,ProjectData.bloc_timing_anim_apparition)

func Changer_Visuel_Oeil(type : String):
	match type:
		"Classique":
			_sprite_oeil.texture = Bibliotheque.oeil_balle_classique
		"Warpzone":
			_sprite_oeil.texture = Bibliotheque.oeil_balle_warpzone
		"Gravite Inversee":
			_sprite_oeil.texture = Bibliotheque.oeil_balle_gravite_inversee
		"Rocket":
			_sprite_oeil.texture = Bibliotheque.oeil_balle_rocket

func Freeze(freeze : bool):
	if freeze :
		if _body != null : _body.process_mode = Node.PROCESS_MODE_DISABLED
	else :
		if _body != null : _body.process_mode = Node.PROCESS_MODE_INHERIT

func Play(nom_animation : String):
	_animation_player.play(nom_animation)

func Rotation_Canon(direction : Vector2):
	var targetRotation = atan2(direction.y, direction.x)
	if _action_tir != null:
		_action_tir.rotation = targetRotation
	if _action_tir_02 != null:
		_action_tir_02.rotation = targetRotation

### PRIVATE METHOD
func _Ajouter_Preview_Prix():
	_preview_prix.visible = true

func _Assignation_Variables():
	var enfants = self.get_children()
	for enfant in enfants :
		if enfant is RigidBody2D or enfant is StaticBody2D:
			_body = enfant
		elif enfant is Control and enfant.name == "Preview_Prix":
			_preview_prix = enfant
		elif enfant.get_meta("Action") == "Cassable":
			_is_cassable = true
			_action_cassable = enfant
			_pv = _action_cassable.pv_initiaux
		elif enfant is AnimationPlayer:
			_animation_player = enfant
	
	if _body != null:
		enfants = _body.get_children()
		for enfant in enfants:
			if enfant is Sprite2D and enfant.get_meta("Sprite") == "Bloc":
				_sprite2D = enfant
			elif enfant is Sprite2D and enfant.get_meta("Sprite") == "Oeil":
				_sprite_oeil = enfant
			elif enfant is Node2D:
				_Actions.append(enfant)
			elif enfant.get_meta("Type")== "Upgrade_System":
				_upgrade_systeme = enfant
				_is_upgradable = true
	
	if _is_cassable:
		enfants = _action_cassable.get_children()
		for enfant in enfants:
			if enfant is Area2D:
				_action_cassable_area2D = enfant
	
	if !self.has_meta("Upgradable") : self.set_meta("Upgradable", false)

func _Changer_Type_Balle(direction : String):
	# UI
	var _current_index = ball_types.find(_current_type_balle)
	
	if direction == "Gauche":
		_current_index = (_current_index -1) % ball_types.size()
	elif direction == "Droite":
		_current_index = (_current_index +1) % ball_types.size()
	else :
		printerr("Mauvaise assignation de direction dans _Changer_Type_Balle du bloc Manager")

	_current_type_balle = ball_types[_current_index]
	_upgrade_systeme.Changer_UI_Balle(_current_type_balle)
	
	# Gameplay
	for action in _Actions:
		if action.get_meta("Action") == "Tir":
			action.Changer_Balle(_current_type_balle)
	
func _Check_If_Actions_Here():
	for action in _Actions:
		if action.get_meta("Action") == "Tir":
			if _action_tir == null :
				_action_tir = action
			else :
				_action_tir_02 = action

func _Degat_Recu():
	if _pv == 3:
		_pv -= 1
		_sprite2D.modulate = Color("a3a3a3")
	elif _pv == 2:
		_pv -= 1
		_sprite2D.modulate = Color("525252")
	else :
		queue_free()

func _on_body_entered(body : RigidBody2D):
	var parent = body.get_parent()
	if parent.get_meta("Type") == "Balle" or parent.get_meta("Type") == "Balle_Rocket":
		if index_joueur == 0 :
			body.add_constant_central_force(ProjectData.force_renvoi_miroir)
			_Degat_Recu()
		elif index_joueur == 1:
			body.add_constant_central_force(-ProjectData.force_renvoi_miroir)
			_Degat_Recu()

func _Set_Up():
	_preview_prix.visible = false
	if _body != null : _body.position = Vector2.ZERO

