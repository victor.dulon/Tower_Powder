extends Area2D

##Replace with Description

### ENUM

### SIGNAL

### EXPORT VARIABLE

### PUBLIC VARIABLE

### PRIVATE VARIABLE
var _survol_objet_upgradable : bool = false
var _index_joueur : int 
var _objet_a_upgrader
var _bouton_B_appuye : bool = false
var _upgrade_en_cours : bool = false

### INHERITED METHOD
func _ready():
	_Assignation_Variables()
	_Connect_Signals()
	pass

### SIGNAL METHOD
func _Connect_Signals():
	self.body_entered.connect(_on_body_entered)
	self.body_exited.connect(_on_body_exited)

func _process(delta):
	if Input.is_joy_button_pressed(_index_joueur,JOY_BUTTON_B) and _survol_objet_upgradable and _bouton_B_appuye == false:
		_bouton_B_appuye = true
		if _objet_a_upgrader != null : 
			StateMachine.Switch_Blocage_Curseur(_index_joueur)
			_objet_a_upgrader._etat_bloc = _objet_a_upgrader.ETAT_BLOC.EN_MODIFICATION
			_objet_a_upgrader.Affiche_Upgrade(true)
			_upgrade_en_cours = !_upgrade_en_cours
			print("Changement d'upgrade en cours : ", _upgrade_en_cours)
			if !_upgrade_en_cours : _objet_a_upgrader._etat_bloc = _objet_a_upgrader.ETAT_BLOC.NEUTRE
		await get_tree().create_timer(0.5).timeout
		_bouton_B_appuye = false

### PUBLIC METHOD

### PRIVATE METHOD
func _Assignation_Variables():
	var parent = self.get_parent()
	if parent.index_joueur : _index_joueur = parent.index_joueur 

func _on_body_entered(body):
	if StateMachine.current_etat_jeu == StateMachine.Etat_Jeu.PLACEMENT:
		var node = body.get_parent()
		if node.get_meta("Upgradable") == true:
			_survol_objet_upgradable = true
			_objet_a_upgrader = node

func _on_body_exited(body):
	if StateMachine.current_etat_jeu == StateMachine.Etat_Jeu.PLACEMENT:
		var node = body.get_parent()
		if node.get_meta("Upgradable") == true:
			_survol_objet_upgradable = true
			_objet_a_upgrader.Affiche_Upgrade(false)
			_objet_a_upgrader._etat_bloc = _objet_a_upgrader.ETAT_BLOC.NEUTRE
			_objet_a_upgrader = null

