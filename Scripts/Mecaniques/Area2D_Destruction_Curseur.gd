extends Area2D

##Replace with Description

### ENUM

### SIGNAL

### EXPORT VARIABLE

### PUBLIC VARIABLE

### PRIVATE VARIABLE

### INHERITED METHOD
func _ready():
	_Connect_Signals()
	pass

### SIGNAL METHOD
func _Connect_Signals():
	self.body_entered.connect(_on_body_entered)

### PUBLIC METHOD
func Desactivation_Temporaire():
	var parent = self.get_parent()
	parent.Descactivation_Temporaire()

### PRIVATE METHOD
func _on_body_entered(body):
	if StateMachine.current_etat_jeu == StateMachine.Etat_Jeu.AFFRONTEMENT:
		var node = body.get_parent()
		if node.get_meta("Type") == "Balle":
			node.Explosion()
			Desactivation_Temporaire()


