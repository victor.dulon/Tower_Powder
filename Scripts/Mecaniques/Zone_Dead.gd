extends Area2D


# Called when the node enters the scene tree for the first time.
func _ready():
	_Connection_Signaux()
	
	
func _Connection_Signaux():
	self.body_entered.connect(_on_body_entered)

func _on_body_entered(body):
	var bloc = body.get_parent()
	if bloc.get_meta("Type") == "Coeur":
		EventManager.Invoke_On_Defaite_Joueur(bloc.index_joueur)
	if bloc.get_meta("Type") == "Balle_Warpzone":		
		var current_velocity = body.linear_velocity
		var current_position = bloc.position
		var arriveeTeleportation : Vector2
		if bloc.index_joueur == 0:
			arriveeTeleportation = Vector2(current_position.x + 1152, current_position.y)
		elif bloc.index_joueur == 1:
			arriveeTeleportation = Vector2(current_position.x - 1152, current_position.y)
		GameManager.Pop_Balle(arriveeTeleportation, current_velocity, "Classique")
		bloc.queue_free
