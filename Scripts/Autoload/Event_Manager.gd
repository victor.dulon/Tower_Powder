extends Node

signal _on_variation_argent(index_joueur : int, variation : int)
func Invoke_On_Variation_Argent(index_joueur : int, variation : int):
	_on_variation_argent.emit(index_joueur, variation)

signal _on_mise_a_jour_argent()
func Invoke_On_Mise_A_Jour_Argent():
	_on_mise_a_jour_argent.emit()

signal _on_commencement_du_tour()
func Invoke_On_Commencement_Du_Tour():
	_on_commencement_du_tour.emit()

signal _on_fin_du_tour()
func Invoke_On_Fin_Du_Tour():
	_on_fin_du_tour.emit()

signal _on_defaite_joueur(index_joueur : int)
func Invoke_On_Defaite_Joueur(index_joueur : int):
	_on_defaite_joueur.emit(index_joueur)

signal _on_nouvelle_partie()
func Invoke_On_Nouvelle_Partie():
	_on_nouvelle_partie.emit()

signal _on_spawn_coeur(index_joueur : int)
func Invoke_On_Spawn_Coeur(index_joueur : int):
	_on_spawn_coeur.emit(index_joueur)

signal _on_choix_terrain(terrain : ProjectData.Type_Terrain, index_joueur : int, selected : bool)
func Invoke_On_Choix_Terrain(terrain : ProjectData.Type_Terrain, index_joueur : int, selected : bool):
	_on_choix_terrain.emit(terrain, index_joueur, selected)
