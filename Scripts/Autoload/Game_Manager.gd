extends Node

var instance_curseur_joueur_01
var instance_curseur_joueur_02
var receiverNode
var coeur_joueur_01 : PackedScene
var coeur_joueur_02 : PackedScene
var instance_coeur_joueur_01 : Node2D
var instance_coeur_joueur_02 : Node2D
var spawn_point_coeur_joueur_01 : Marker2D
var spawn_point_coeur_joueur_02 : Marker2D

## Inherit
# Called when the node enters the scene tree for the first time.
func _ready():
	if StateMachine.current_etat_jeu == StateMachine.Etat_Jeu.AFFRONTEMENT:
		_Assignations_Nodes()
		_Initialisation_Scene()
	_Connexion_Signaux()

## Public

func Apply_Joy_Deadzone(vec : Vector2)-> Vector2:
	var _deadzone = ProjectData.deadzone
	if abs(vec.x) < _deadzone:
		vec.x = 0.0
	else:
		vec.x = (vec.x - _deadzone) / (1.0 - _deadzone)

	if abs(vec.y) < _deadzone:
		vec.y = 0.0
	else:
		vec.y = (vec.y - _deadzone) / (1.0 - _deadzone)

	return vec

func Create_Bloc(_position : Vector2, nom_instance : String, rotation_canon : Vector2, index_joueur : int):
	var instance = Bibliotheque.Instance_By_Name(nom_instance).instantiate()
	if receiverNode != null:
		instance.global_position = _position
		receiverNode.add_child(instance)
		instance.Animation_Apparition()
		if instance._animation_player != null : instance.Play("Idle")
		instance.index_joueur = index_joueur
		instance.Rotation_Canon(rotation_canon)

func Switch_Visibility_Curseur(index_joueur : int):
	if index_joueur == 0:
		instance_curseur_joueur_01.visible = !instance_curseur_joueur_01.visible
	elif index_joueur == 1:
		instance_curseur_joueur_02.visible = !instance_curseur_joueur_02.visible

func Get_Prix_Grace_Aux_Metas(node) -> int:
	if node.get_meta("Type") == "Tireur":
		return ProjectData.prix_bloc_tireur
	elif node.get_meta("Type") == "Tireur_B":
		return ProjectData.prix_bloc_tireur_B
	elif node.get_meta("Type") == "Mur":
		return ProjectData.prix_bloc_mur
	elif node.get_meta("Type") == "Mur_B":
		return ProjectData.prix_bloc_mur_B
	elif node.get_meta("Type") == "Mur_C":
		return ProjectData.prix_bloc_mur_C
	elif node.get_meta("Type") == "Mur_D":
		return ProjectData.prix_bloc_mur_D
	elif node.get_meta("Type") == "Miroir":
		return ProjectData.prix_bloc_miroir
	elif node.get_meta("Type") == "Coeur":
		return ProjectData.prix_bloc_coeur
	elif node.get_meta("Type") == "Plateforme":
		return ProjectData.prix_bloc_plateforme
	else :
		printerr("La preview de prix n'arrive pas à savoir de quel node il doit chercher le prix. Resultat : ",node.get_meta("Type"))
		return 0

func Go_To_Affrontement():
	get_tree().change_scene_to_packed(Bibliotheque.scene_affrontement)
	Set_Variables_En_Fonction_Choix_Terrains()
	Reinitialisation_Runtime_Data()
	EventManager.Invoke_On_Mise_A_Jour_Argent()

func Go_To_Choix_Terrain():
	get_tree().change_scene_to_packed(Bibliotheque.scene_choix_terrain)
	RuntimeData.terrain_joueur_01 = 0
	RuntimeData.terrain_joueur_02 = 0
	Nouvelle_Partie()
	StateMachine.Set_Etat_Jeu(StateMachine.Etat_Jeu.CHOIX_TERRAIN)

	_Clean_Game_Manager()

func Nouvelle_Partie():
	_on_phase_placement()
	Reinitialisation_Runtime_Data()
	Suppression_Des_Blocs()
	EventManager.Invoke_On_Mise_A_Jour_Argent()
	#SpawnCoeurs()

func Pop_Balle(_origine : Vector2, _vitesse : Vector2, _type : String):
	var nouvelle_balle = Type_Balle_Depuis_Nom(_type).instantiate()
	var enfants = nouvelle_balle.get_children()
	var rigid_body
	for enfant in enfants :
		if enfant is RigidBody2D:
			rigid_body = enfant
	nouvelle_balle.position = _origine
	receiverNode.add_child(nouvelle_balle)
	rigid_body.linear_velocity = _vitesse

func Reinitialisation_Runtime_Data():
	RuntimeData.argent_joueur_01 = ProjectData.argent_depart_joueur_01
	RuntimeData.argent_joueur_02 = ProjectData.argent_depart_joueur_02
	RuntimeData.puissance_tir_joueur_01 = ProjectData.puissance_tir_joueur_01
	RuntimeData.puissance_tir_joueur_02 = ProjectData.puissance_tir_joueur_02
	RuntimeData.action_tir_cool_down = ProjectData.action_tir_cool_down
	RuntimeData.temps_restant = ProjectData.temps_restant

func Set_Variables_En_Fonction_Choix_Terrains():
	match RuntimeData.terrain_joueur_01:
		1: ProjectData.argent_depart_joueur_01 = ProjectData.revenus_terrain_01
		2: ProjectData.argent_depart_joueur_01 = ProjectData.revenus_terrain_02
		3: ProjectData.argent_depart_joueur_01 = ProjectData.revenus_terrain_03
		4: ProjectData.argent_depart_joueur_01 = ProjectData.revenus_terrain_04
		5: ProjectData.argent_depart_joueur_01 = ProjectData.revenus_terrain_05
		6: ProjectData.argent_depart_joueur_01 = ProjectData.revenus_terrain_06
		7: ProjectData.argent_depart_joueur_01 = ProjectData.revenus_terrain_07
		8: ProjectData.argent_depart_joueur_01 = ProjectData.revenus_terrain_08
		9: ProjectData.argent_depart_joueur_01 = ProjectData.revenus_terrain_09
	match RuntimeData.terrain_joueur_02:
		1: ProjectData.argent_depart_joueur_02 = ProjectData.revenus_terrain_01
		2: ProjectData.argent_depart_joueur_02 = ProjectData.revenus_terrain_02
		3: ProjectData.argent_depart_joueur_02 = ProjectData.revenus_terrain_03
		4: ProjectData.argent_depart_joueur_02 = ProjectData.revenus_terrain_04
		5: ProjectData.argent_depart_joueur_02 = ProjectData.revenus_terrain_05
		6: ProjectData.argent_depart_joueur_02 = ProjectData.revenus_terrain_06
		7: ProjectData.argent_depart_joueur_02 = ProjectData.revenus_terrain_07
		8: ProjectData.argent_depart_joueur_02 = ProjectData.revenus_terrain_08
		9: ProjectData.argent_depart_joueur_02 = ProjectData.revenus_terrain_09

func Start_Scene_Affrontement():
	_Assignations_Nodes()
	_Initialisation_Scene()
	var _instance_terrain_joueur_01 = Bibliotheque.Get_Instance_Terrain(0).instantiate()
	var _instance_terrain_joueur_02 = Bibliotheque.Get_Instance_Terrain(1).instantiate()
	receiverNode.add_child(_instance_terrain_joueur_01)
	receiverNode.add_child(_instance_terrain_joueur_02)

func Suppression_Des_Blocs():
	var enfants = receiverNode.get_children()
	for enfant in enfants :
		enfant.queue_free()

func Type_Balle_Depuis_Nom(_nom : String) -> PackedScene:
	if _nom == "Classique":
		return Bibliotheque.balle_classique
	elif _nom == "Gravite Inversee":
		return Bibliotheque.balle_gravite_inversee
	elif _nom == "Warpzone":
		return Bibliotheque.balle_warpzone
	elif _nom == "Rocket":
		return Bibliotheque.balle_rocket
	else :
		printerr("Type de balle inexistant")
		return null

## Private

func _Assignations_Nodes():
	receiverNode = get_tree().get_first_node_in_group("Receveur_Instance")
	coeur_joueur_01 = Bibliotheque.bloc_coeur
	coeur_joueur_02 = Bibliotheque.bloc_coeur
	var markers = get_tree().get_nodes_in_group("Spawn_Point")
	for marker in markers :
		if marker.get_meta("index_joueur") == 0:
			spawn_point_coeur_joueur_01 = marker
		elif marker.get_meta("index_joueur") == 1:
			spawn_point_coeur_joueur_02 = marker
		else :
			printerr("Erreur d'index_joueur dans le meta du node : ", marker)

func _Clean_Game_Manager():
	var enfants = self.get_children()
	for enfant in enfants : 
		enfant.queue_free()

func _Connexion_Signaux():
	EventManager._on_variation_argent.connect(_on_variation_argent)
	EventManager._on_commencement_du_tour.connect(_on_commencement_du_tour)
	EventManager._on_fin_du_tour.connect(_on_fin_du_tour)
	EventManager._on_nouvelle_partie.connect(Nouvelle_Partie)
	EventManager._on_defaite_joueur.connect(_on_defaite_joueur)
	EventManager._on_spawn_coeur.connect(_on_spawn_coeur)
	EventManager._on_choix_terrain.connect(_on_choix_terrain)

func _Import_Curseurs():
	instance_curseur_joueur_01 = Bibliotheque.player_curseur.instantiate()
	instance_curseur_joueur_02 = Bibliotheque.player_curseur.instantiate()
	
	instance_curseur_joueur_01.index_joueur = 0
	instance_curseur_joueur_02.index_joueur = 1
	
	add_child(instance_curseur_joueur_01)
	add_child(instance_curseur_joueur_02)
	
	instance_curseur_joueur_01.position = spawn_point_coeur_joueur_01.global_position
	instance_curseur_joueur_02.position = spawn_point_coeur_joueur_02.global_position

func _Initialisation_Scene():
	_Import_Curseurs()
	EventManager.Invoke_On_Mise_A_Jour_Argent()

func _on_choix_terrain(terrain : ProjectData.Type_Terrain, index_joueur : int, selected : bool):
	if index_joueur == 0 :
		if selected :
			RuntimeData.terrain_joueur_01 = terrain
		else :
			RuntimeData.terrain_joueur_01 = 0
	elif index_joueur == 1:
		if selected :
			RuntimeData.terrain_joueur_02 = terrain
		else :
			RuntimeData.terrain_joueur_02 = 0
	else :
		printerr("Mauvais index_joueur dans le _on_choix_terrain du GameManager")
	
	if RuntimeData.terrain_joueur_01 != 0 and RuntimeData.terrain_joueur_02 != 0:
		GameManager.Go_To_Affrontement()

func _on_commencement_du_tour():
	StateMachine.Set_Etat_Jeu(StateMachine.Etat_Jeu.AFFRONTEMENT)
	instance_curseur_joueur_01.position = spawn_point_coeur_joueur_01.global_position
	instance_curseur_joueur_02.position = spawn_point_coeur_joueur_02.global_position

func _on_defaite_joueur(index_joueur : int):
	StateMachine.Set_Etat_Jeu(StateMachine.Etat_Jeu.ECRAN_GAGNANT)

func _on_fin_du_tour():
	RuntimeData.temps_restant = ProjectData.temps_restant
	RuntimeData.argent_joueur_01 += ProjectData.argent_depart_joueur_01
	RuntimeData.argent_joueur_02 += ProjectData.argent_depart_joueur_02
	EventManager.Invoke_On_Mise_A_Jour_Argent()
	StateMachine.Set_Etat_Jeu(StateMachine.Etat_Jeu.PLACEMENT)

func _on_phase_placement():
	StateMachine.Set_Etat_Jeu(StateMachine.Etat_Jeu.PLACEMENT)

func _on_spawn_coeur(index_joueur:int):
	if index_joueur == 0:
		RuntimeData.joueur_01_coeur_spawned = true
	elif index_joueur == 1:
		RuntimeData.joueur_02_coeur_spawned = true
	else :
		printerr("Attention, mauvais index_joueur dans le spawn de coeur !")
	
	if RuntimeData.joueur_01_coeur_spawned and RuntimeData.joueur_02_coeur_spawned:
		RuntimeData.game_ready_car_coeurs_spawned = true

func _on_variation_argent(index_joueur : int, depense : int):
	if index_joueur == 0:
		RuntimeData.argent_joueur_01 += depense
	elif index_joueur == 1:
		RuntimeData.argent_joueur_02 += depense
	EventManager.Invoke_On_Mise_A_Jour_Argent()
