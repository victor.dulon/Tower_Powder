extends Node2D

# Controllers
var player_curseur : PackedScene = load("res://Assets/Prefab/Control/Player_Curseur.tscn")

# Scenes
var scene_affrontement : PackedScene = load("res://Scene/Main_Scene.tscn")
var scene_choix_terrain : PackedScene = load("res://Scene/Choix_Terrain.tscn")

# Blocs 
var bloc_mur : PackedScene = load("res://Assets/Prefab/Blocs/Bloc_Mur.tscn")
var bloc_mur_B : PackedScene = load("res://Assets/Prefab/Blocs/Bloc_Mur_B.tscn")
var bloc_mur_C : PackedScene = load("res://Assets/Prefab/Blocs/Bloc_Mur_C.tscn")
var bloc_mur_D : PackedScene = load("res://Assets/Prefab/Blocs/Bloc_Mur_D.tscn")
var bloc_mur_D_Droite : PackedScene = load("res://Assets/Prefab/Blocs/Bloc_Mur_D_Droite.tscn")
var bloc_tireur : PackedScene = load("res://Assets/Prefab/Blocs/Bloc_Tireur.tscn")
var bloc_tireur_B : PackedScene = load("res://Assets/Prefab/Blocs/Bloc_Tireur_B.tscn")
var bloc_miroir : PackedScene = load("res://Assets/Prefab/Blocs/Bloc_Miroir.tscn")
var bloc_coeur : PackedScene = load("res://Assets/Prefab/Blocs/Bloc_Coeur.tscn")
var bloc_plateforme : PackedScene = load("res://Assets/Prefab/Blocs/Bloc_Plateforme.tscn")

# Balles
var balle_classique : PackedScene = load("res://Assets/Prefab/Balles/Balle_Classique.tscn")
var balle_gravite_inversee : PackedScene = load("res://Assets/Prefab/Balles/Balle_Gravite_inversee.tscn")
var balle_warpzone : PackedScene = load("res://Assets/Prefab/Balles/Balle_WarpZone.tscn")
var balle_rocket : PackedScene = load("res://Assets/Prefab/Balles/Balle_Rocket.tscn")

# UI Balles
var ui_balle_classique : CompressedTexture2D = load("res://Assets/Sprites/UI/Upgrade/Symboles_Balles_Classique.png")
var ui_balle_gravite_inversee : CompressedTexture2D = load("res://Assets/Sprites/UI/Upgrade/Symboles_Balles_Gravite_Inversee.png")
var ui_balle_warpzone : CompressedTexture2D = load("res://Assets/Sprites/UI/Upgrade/Symboles_Balles_Warpzone.png")
var ui_balle_rocket : CompressedTexture2D = load("res://Assets/Sprites/UI/Upgrade/Symboles_Balles_Rocket.png")

var oeil_balle_classique : CompressedTexture2D = load("res://Assets/Sprites/Blocs/Tireur/Yeux_Classique.png")
var oeil_balle_gravite_inversee : CompressedTexture2D = load("res://Assets/Sprites/Blocs/Tireur/Yeux_GraviteInverse.png")
var oeil_balle_warpzone : CompressedTexture2D = load("res://Assets/Sprites/Blocs/Tireur/Yeux_Warpzone.png")
var oeil_balle_rocket : CompressedTexture2D = load("res://Assets/Sprites/Blocs/Tireur/Yeux_Rocket.png")

# ------------ Terrains --------------
# Terrain 01
var terrain_01_button_image : CompressedTexture2D = load("res://Assets/Sprites/UI/Choix_Terrain/Bouton_Choix_Terrain_01.png")
var terrain_01_button_focus_image : CompressedTexture2D = load("res://Assets/Sprites/UI/Choix_Terrain/Bouton_Choix_Terrain_01_Focused.png")
var terrain_01_button_selected_image : CompressedTexture2D = load("res://Assets/Sprites/UI/Choix_Terrain/Bouton_Choix_Terrain_01_Selected.png")
var terrain_01_joueur_01_prefab : PackedScene = load("res://Assets/Prefab/Terrains/Terrain_01_Joueur_01.tscn")
var terrain_01_joueur_02_prefab : PackedScene = load("res://Assets/Prefab/Terrains/Terrain_01_Joueur_02.tscn")
# Terrain 01
var terrain_02_button_image : CompressedTexture2D = load("res://Assets/Sprites/UI/Choix_Terrain/Bouton_Choix_Terrain_02.png")
var terrain_02_button_focus_image : CompressedTexture2D = load("res://Assets/Sprites/UI/Choix_Terrain/Bouton_Choix_Terrain_02_Focused.png")
var terrain_02_button_selected_image : CompressedTexture2D = load("res://Assets/Sprites/UI/Choix_Terrain/Bouton_Choix_Terrain_02_Selected.png")
var terrain_02_joueur_01_prefab : PackedScene = load("res://Assets/Prefab/Terrains/Terrain_02_Joueur_01.tscn")
var terrain_02_joueur_02_prefab : PackedScene = load("res://Assets/Prefab/Terrains/Terrain_02_Joueur_02.tscn")
# Terrain 03
var terrain_03_button_image : CompressedTexture2D = load("res://Assets/Sprites/UI/Choix_Terrain/Bouton_Choix_Terrain_03.png")
var terrain_03_button_focus_image : CompressedTexture2D = load("res://Assets/Sprites/UI/Choix_Terrain/Bouton_Choix_Terrain_03_Focused.png")
var terrain_03_button_selected_image : CompressedTexture2D = load("res://Assets/Sprites/UI/Choix_Terrain/Bouton_Choix_Terrain_03_Selected.png")
var terrain_03_joueur_01_prefab : PackedScene = load("res://Assets/Prefab/Terrains/Terrain_03_Joueur_01.tscn")
var terrain_03_joueur_02_prefab : PackedScene = load("res://Assets/Prefab/Terrains/Terrain_03_Joueur_02.tscn")
# Terrain 04
var terrain_04_button_image : CompressedTexture2D = load("res://Assets/Sprites/UI/Choix_Terrain/Bouton_Choix_Terrain_04.png")
var terrain_04_button_focus_image : CompressedTexture2D = load("res://Assets/Sprites/UI/Choix_Terrain/Bouton_Choix_Terrain_04_Focused.png")
var terrain_04_button_selected_image : CompressedTexture2D = load("res://Assets/Sprites/UI/Choix_Terrain/Bouton_Choix_Terrain_04_Selected.png")
var terrain_04_joueur_01_prefab : PackedScene = load("res://Assets/Prefab/Terrains/Terrain_04_Joueur_01.tscn")
var terrain_04_joueur_02_prefab : PackedScene = load("res://Assets/Prefab/Terrains/Terrain_04_Joueur_02.tscn")
# Terrain 05
var terrain_05_button_image : CompressedTexture2D = load("res://Assets/Sprites/UI/Choix_Terrain/Bouton_Choix_Terrain_05.png")
var terrain_05_button_focus_image : CompressedTexture2D = load("res://Assets/Sprites/UI/Choix_Terrain/Bouton_Choix_Terrain_05_Focused.png")
var terrain_05_button_selected_image : CompressedTexture2D = load("res://Assets/Sprites/UI/Choix_Terrain/Bouton_Choix_Terrain_05_Selected.png")
var terrain_05_joueur_01_prefab : PackedScene = load("res://Assets/Prefab/Terrains/Terrain_05_Joueur_01.tscn")
var terrain_05_joueur_02_prefab : PackedScene = load("res://Assets/Prefab/Terrains/Terrain_05_Joueur_02.tscn")
# Terrain 06
var terrain_06_button_image : CompressedTexture2D = load("res://Assets/Sprites/UI/Choix_Terrain/Bouton_Choix_Terrain_06.png")
var terrain_06_button_focus_image : CompressedTexture2D = load("res://Assets/Sprites/UI/Choix_Terrain/Bouton_Choix_Terrain_06_Focused.png")
var terrain_06_button_selected_image : CompressedTexture2D = load("res://Assets/Sprites/UI/Choix_Terrain/Bouton_Choix_Terrain_06_Selected.png")
var terrain_06_joueur_01_prefab : PackedScene = load("res://Assets/Prefab/Terrains/Terrain_06_Joueur_01.tscn")
var terrain_06_joueur_02_prefab : PackedScene = load("res://Assets/Prefab/Terrains/Terrain_06_Joueur_02.tscn")
# Terrain 07
var terrain_07_button_image : CompressedTexture2D = load("res://Assets/Sprites/UI/Choix_Terrain/Bouton_Choix_Terrain_07.png")
var terrain_07_button_focus_image : CompressedTexture2D = load("res://Assets/Sprites/UI/Choix_Terrain/Bouton_Choix_Terrain_07_Focused.png")
var terrain_07_button_selected_image : CompressedTexture2D = load("res://Assets/Sprites/UI/Choix_Terrain/Bouton_Choix_Terrain_07_Selected.png")
var terrain_07_joueur_01_prefab : PackedScene = load("res://Assets/Prefab/Terrains/Terrain_07_Joueur_01.tscn")
var terrain_07_joueur_02_prefab : PackedScene = load("res://Assets/Prefab/Terrains/Terrain_07_Joueur_02.tscn")
# Terrain 08
var terrain_08_button_image : CompressedTexture2D = load("res://Assets/Sprites/UI/Choix_Terrain/Bouton_Choix_Terrain_08.png")
var terrain_08_button_focus_image : CompressedTexture2D = load("res://Assets/Sprites/UI/Choix_Terrain/Bouton_Choix_Terrain_08_Focused.png")
var terrain_08_button_selected_image : CompressedTexture2D = load("res://Assets/Sprites/UI/Choix_Terrain/Bouton_Choix_Terrain_08_Selected.png")
var terrain_08_joueur_01_prefab : PackedScene = load("res://Assets/Prefab/Terrains/Terrain_08_Joueur_01.tscn")
var terrain_08_joueur_02_prefab : PackedScene = load("res://Assets/Prefab/Terrains/Terrain_08_Joueur_02.tscn")
# Terrain 09
var terrain_09_button_image : CompressedTexture2D = load("res://Assets/Sprites/UI/Choix_Terrain/Bouton_Choix_Terrain_09.png")
var terrain_09_button_focus_image : CompressedTexture2D = load("res://Assets/Sprites/UI/Choix_Terrain/Bouton_Choix_Terrain_09_Focused.png")
var terrain_09_button_selected_image : CompressedTexture2D = load("res://Assets/Sprites/UI/Choix_Terrain/Bouton_Choix_Terrain_09_Selected.png")
var terrain_09_joueur_01_prefab : PackedScene = load("res://Assets/Prefab/Terrains/Terrain_09_Joueur_01.tscn")
var terrain_09_joueur_02_prefab : PackedScene = load("res://Assets/Prefab/Terrains/Terrain_09_Joueur_02.tscn")

var terrain_normal_color : Color = Color.WHITE
var terrain_focus_color : Color = Color("70add7")
var terrain_selected_color : Color = Color("79a84a")

# Functions
func Instance_By_Name(nom : String)-> PackedScene :
	if nom == "Mur":
		return bloc_mur
	elif nom == "Mur_B":
		return bloc_mur_B
	elif nom == "Mur_C":
		return bloc_mur_C
	elif nom == "Mur_D":
		return bloc_mur_D
	elif nom == "Tireur":
		return bloc_tireur
	elif nom == "Tireur_B":
		return bloc_tireur_B
	elif nom == "Coeur":
		return bloc_coeur
	elif nom == "Plateforme":
		return bloc_plateforme
	else :
		printerr("Mauvais nom entré dans 'Instance_By_Name' de la bibliotheque")
		return null

func Get_Instance_Terrain(index_joueur) -> PackedScene:
	if index_joueur == 0:
		match RuntimeData.terrain_joueur_01:
			1 : return terrain_01_joueur_01_prefab
			2 : return terrain_02_joueur_01_prefab
			3 : return terrain_03_joueur_01_prefab
			4 : return terrain_04_joueur_01_prefab
			5 : return terrain_05_joueur_01_prefab
			6 : return terrain_06_joueur_01_prefab
			7 : return terrain_07_joueur_01_prefab
			8 : return terrain_08_joueur_01_prefab
			9 : return terrain_09_joueur_01_prefab
			_ : return terrain_01_joueur_01_prefab
	else :
		match RuntimeData.terrain_joueur_02:
			1 : return terrain_01_joueur_02_prefab
			2 : return terrain_02_joueur_02_prefab
			3 : return terrain_03_joueur_02_prefab
			4 : return terrain_04_joueur_02_prefab
			5 : return terrain_05_joueur_02_prefab
			6 : return terrain_06_joueur_02_prefab
			7 : return terrain_07_joueur_02_prefab
			8 : return terrain_08_joueur_02_prefab
			9 : return terrain_09_joueur_02_prefab
			_ : return terrain_01_joueur_02_prefab
