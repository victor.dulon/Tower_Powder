extends Node

# Money
var argent_joueur_01 : int = ProjectData.argent_depart_joueur_01
var argent_joueur_02 : int = ProjectData.argent_depart_joueur_02


# Caractéristiques actions
var action_tir_cool_down : float = ProjectData.action_tir_cool_down
var puissance_tir_joueur_01 : int = ProjectData.puissance_tir_joueur_01
var puissance_tir_joueur_02 : int = ProjectData.puissance_tir_joueur_02

# Temps
var temps_restant : int = ProjectData.temps_restant

# Joueurs
var joueur_01_coeur_spawned : bool = false
var joueur_02_coeur_spawned : bool = false
var game_ready_car_coeurs_spawned : bool = false
var terrain_joueur_01 : ProjectData.Type_Terrain
var terrain_joueur_02 : ProjectData.Type_Terrain
