extends Node2D

# Controler 
var vitesse_mouvement_controlleur : int = 400
var deadzone : float = 0.15

# Blocs
var bloc_scale_anim_apparition : Vector2 = Vector2(1.2,1.2)
var bloc_timing_anim_apparition : float = 0.1
var puissance_tir_joueur_01 : int = 10
var puissance_tir_joueur_02 : int = 10
var action_tir_cool_down : float = 2.0
var force_renvoi_miroir : Vector2 = Vector2(600,600)

# Money
var argent_depart_joueur_01 : int = 300
var argent_depart_joueur_02 : int = 300
var prix_bloc_mur : int = 50
var prix_bloc_mur_B : int = 100
var prix_bloc_mur_C : int = 50
var prix_bloc_mur_D : int = 150
var prix_bloc_tireur : int = 100
var prix_bloc_tireur_B : int = 175
var prix_bloc_miroir : int = 100
var prix_bloc_coeur : int = 0
var prix_bloc_plateforme : int = 200
var gain_argent_chaque_tour_joueur_01 : int = 300
var gain_argent_chaque_tour_joueur_02 : int = 300

# Temps
var temps_restant : int = 15

# Terrains
enum Type_Terrain {RIEN = 0, TERRAIN_01 = 1, TERRAIN_02 = 2, TERRAIN_03 = 3, TERRAIN_04 = 4,TERRAIN_05 = 5, TERRAIN_06 = 6, TERRAIN_07 = 7, TERRAIN_08 = 8, TERRAIN_09 = 9}
var revenus_terrain_01 : int = 250 
var revenus_terrain_02 : int = 300 
var revenus_terrain_03 : int = 350 
var revenus_terrain_04 : int = 200 
var revenus_terrain_05 : int = 300 
var revenus_terrain_06 : int = 225
var revenus_terrain_07 : int = 300 
var revenus_terrain_08 : int = 100 
var revenus_terrain_09 : int = 150 

# Balles
var ball_types = ["Classique", "Warpzone", "Gravite Inversee", "Rocket"]






