extends Node

# Etat Jeu
var current_etat_jeu : Etat_Jeu = Etat_Jeu.PLACEMENT
enum Etat_Jeu {MAIN_MENU = 0, CHOIX_TERRAIN = 3, PLACEMENT = 6, AFFRONTEMENT = 12, ECRAN_GAGNANT = 30, PAUSE = 90}
func Set_Etat_Jeu(etat_jeu : Etat_Jeu):
	current_etat_jeu = etat_jeu

# Etat Curseur
enum Etat_Curseur {LIBRE = 0, BLOQUE = 5, DESACTIVE = 10}

var current_etat_curseur_joueur_01 : Etat_Curseur = Etat_Curseur.LIBRE
var current_etat_curseur_joueur_02 : Etat_Curseur = Etat_Curseur.LIBRE
func Set_Etat_Curseur(etat_curseur : Etat_Curseur, index_joueur):
	if index_joueur == 0:
		current_etat_curseur_joueur_01 = etat_curseur
	elif index_joueur == 1:
		current_etat_curseur_joueur_02 = etat_curseur
	else : 
		printerr("Mauvais index_joueur dans la fonction Set_Etat_Curseur du State Manager")
	
func Switch_Blocage_Curseur(index_joueur):
	if Get_State_Curseur_Depuis_Index(index_joueur) == Etat_Curseur.LIBRE :
		Set_Etat_Curseur(Etat_Curseur.BLOQUE, index_joueur)
	else :
		Set_Etat_Curseur(Etat_Curseur.LIBRE, index_joueur)
	GameManager.Switch_Visibility_Curseur(index_joueur)

func Get_State_Curseur_Depuis_Index(index_joueur : int)-> Etat_Curseur:
	if index_joueur == 0:
		return current_etat_curseur_joueur_01
	elif index_joueur == 1 :
		return current_etat_curseur_joueur_02
	else :
		printerr("Mauvais index_joueur dans Get_State_Curseur_Depuis_Index du StateMachine")
		return current_etat_curseur_joueur_01
